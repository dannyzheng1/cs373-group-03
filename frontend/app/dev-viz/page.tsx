// @ts-nocheck
"use client";

import React, { useState, useEffect } from "react";
import LinePlot from "./lineplot";
import { Button } from "@/components/ui/button";

import Error from "@/components/Error";

import axios from "axios";
import DataMap from "./datamap";
import { BarChart } from "./barchart";
// need to create data visualizations, and stack them on
// top of each other

const DEVELOPER_URL = "https://api.unshakablejapan.me/";

function Page() {
  const [error, setError] = useState<boolean>(false);

  // Start of Developer Data Visualizations
  type Prefecture = {
    area: number;
    area_code: string;
    capital_city: string;
    density: number;
    id: number;
    island: string;
    iso: string;
    latitude: number;
    longitude: number;
    name: string;
    num_districts: number;
    num_municipalities: number;
    population: number;
    risk: number;
  };

  const [prefecturePop, setPrefecturePop] = useState<number[]>([]);
  const [prefectureRisk, setPrefectureRisk] = useState<number[]>([]);
  const [prefectureLabels, setPrefectureLabels] = useState<string[]>([]);

  function comparePrefecture(
    prefectureOne: Prefecture,
    prefectureTwo: Prefecture
  ) {
    return prefectureOne.risk - prefectureTwo.risk;
  }

  useEffect(() => {
    // grabs all the counties, sort them by population rank
    // literally an array
    axios.get(`${DEVELOPER_URL}/prefectures`).then((res) => {
      // grabs all the counties, sort them by population rank
      // literally an array
      const prefectureArray: Prefecture[] = res.data;
      const prefectureLabels: string[] = [];
      const prefectureRisk: number[] = [];

      prefectureArray.sort(comparePrefecture);

      const prefecturePopSorted: number[] = prefectureArray.map(
        (prefecture) => {
          prefectureLabels.push(`Name: ${prefecture.name}. 
                                Risk: ${prefecture.risk}. 
                                Population: ${prefecture.population}`);
          prefectureRisk.push(prefecture.risk);
          return prefecture.population;
        }
      );

      setPrefectureRisk(prefectureRisk);
      setPrefecturePop(prefecturePopSorted);
      setPrefectureLabels(prefectureLabels);
    });
  }, []);

  type Earthquake = {
    casualties: number;
    date: string;
    id: number;
    latitude: float;
    longitude: float;
    magnitude: float;
    name: string;
    prefecture_id: number;
    year: number;
  };

  const [earthQuakeData, setEarthquakeData] = useState<Earthquake[]>([]);
  const [locations, setLocations] = useState<Object[]>([]);
  useEffect(() => {
    axios.get(`${DEVELOPER_URL}/earthquakes`).then((res) => {
      const earthquakeArray: Earthquake[] = res.data;
      setEarthquakeData(earthquakeArray);
      let parse = earthquakeArray.map((data: any) => {
        return {
          name: data.name,
          id: data.prefecture_id,
          location: [data.longitude, data.latitude],
          magnitude: data.magnitude,
          color:
            data.magnitude > 6
              ? data.magnitude > 8
                ? "Red"
                : "Orange"
              : "Yellow",
        };
      });
      setLocations(parse);
      // //Richter scale from 1-9, 9 spots
      // let mag = [0, 0, 0, 0, 0, 0, 0, 0, 0];

      // for (const earthquake of earthquakeArray) {
      //   // resourceLabel.push(`${resource.name} `);
      //   let index = Math.floor(earthquake.magnitude);
      //   mag[index - 1]++;
      // }

      // setEarthquakeData([
      //   { name: "1", value: mag[0] },
      //   { name: "2", value: mag[1] },
      //   { name: "3", value: mag[2] },
      //   { name: "4", value: mag[3] },
      //   { name: "5", value: mag[4] },
      //   { name: "6", value: mag[5] },
      //   { name: "7", value: mag[6] },
      //   { name: "8", value: mag[7] },
      //   { name: "9", value: mag[8] },
      // ]);
    });
  }, []);

  type Resources = {
    disaster_types: string;
    id: number;
    image: string;
    info_type: string;
    languages: string;
    link: string;
    name: string;
    percent_coverage: number;
    prefectures: Object[];
  };

  // resource labels will be name + link
  // const [resourceLabel, setResourceLabel] = useState<string[]>([]);
  const [resourceInfoTypes, setResourceInfoTypes] = useState<IData[]>([]);

  useEffect(() => {
    axios.get(`${DEVELOPER_URL}/resources`).then((res) => {
      const resourcesArray: Resources[] = res.data;
      const infotypeToCount: Map<string, number> = new Map();

      // fill out map, mapping info types to occurrences of info type
      for (const resource of resourcesArray) {
        // resourceLabel.push(`${resource.name} `);

        const currentInfoCount = infotypeToCount.get(resource.info_type);
        if (currentInfoCount != undefined) {
          infotypeToCount.set(resource.info_type, currentInfoCount + 1);
        } else {
          infotypeToCount.set(resource.info_type, 1);
        }
      }
      let keys = Array.from(infotypeToCount.keys());
      setResourceInfoTypes([
        { name: keys[0], value: infotypeToCount.get(keys[0]) },
        { name: keys[1], value: infotypeToCount.get(keys[1]) },
        { name: keys[2], value: infotypeToCount.get(keys[2]) },
        { name: keys[3], value: infotypeToCount.get(keys[3]) },
        { name: keys[4], value: infotypeToCount.get(keys[4]) },
        { name: keys[5], value: infotypeToCount.get(keys[5]) },
        { name: keys[6], value: infotypeToCount.get(keys[6]) },
        { name: keys[7], value: infotypeToCount.get(keys[7]) },
      ]);
      /** 
      let resourcesBarChartData: IData[] = [];

      for (const infotype in infotypeToCount.keys()) {
        const infotypeCount = infotypeToCount.get(infotype);
        resourcesBarChartData.push({ name: infotype, value: infotypeCount });
      }
      setResourceInfoTypes(resourcesBarChartData);
      */
    });
  }, []);

  if (error) {
    return <Error />;
  }

  return (
    <div className="space-y-2">
      <div>
        <h2 className="text-2xl font-semibold">
          Critiques On The Developer Team
        </h2>
        <br></br>
        <div>
          <p className="font-semibold">
            What did they do well?
          </p>
          <br></br>
          Their website looks very polished and aesthetic. It also provides the
          info about their underrepresented community efficiently. They
          communicated well with us and gave good feedback on improving our
          website.
          <br></br>
          <br></br>
          <p className="font-semibold">
            How effective was their RESTful API?
          </p>
          <br></br>
          Their API was well-documented and very effective. They provided
          examples of each API call, making it easy to understand.
          <br></br>
          <br></br>
          <p className="font-semibold">
            How well did they implement your user stories?
          </p>
          <br></br>
          They quickly assigned each story to one of their members and
          implemented it promptly. They always communicated about their
          progress.
          <br></br>
          <br></br>
          <p className="font-semibold">
            What did we learn from their website?
          </p>
          <br></br>
          We learned about the severity and frequency of earthquakes in Japan,
          the resources people can reach out to, and what to do in case of an
          earthquake. We also learned about the prefectures in Japan that are
          more at risk for these earthquakes.
          <br></br>
          <br></br>
          <p className="font-semibold">
            What can they do better?
          </p>
          <br></br>
          They could make their global search accessible from any page by adding
          it to the navigation bar. Their global search is only on the splash
          page. The cards can have images on them to make the site more lively.
          <br></br>
          <br></br>
          <p className="font-semibold">
            What puzzles us about their website?
          </p>
          <br></br>
          What puzzled us about their website was why the earthquake instances
          only have one prefecture for their related instance. We are intrigued
          about whether it only hit that prefecture or was the prefecture most
          impacted. It wouldn&apos;t make sense for only one prefecture to be
          affected by an earthquake.
        </div>
      </div>
      <br></br>
      <p className="text-2xl font-semibold">
        Our Developer Team&apos;s Visualizations
      </p>
      <br></br>
      <p className="font-semibold">Prefecture Population vs Risk</p>
      <LinePlot
        name={"Prefecture Population vs Risk"}
        verticalData={prefecturePop}
        horizontalData={prefectureRisk}
        dataLabels={prefectureLabels}
        width={740}
        height={740}
        marginLeft={60}
      ></LinePlot>
      <br></br>
      <p className="font-semibold">Bar Chart of Types of Information Resources and Their Counts</p>
      <BarChart
        data={resourceInfoTypes}
        marginleft={40}
        fill="green"
        width={1200}
      ></BarChart>
      <br></br>
      <div className="font-semibold">  Map of All Earthquake Locations Colored by Magnitude</div>
      <div>
        <div className="bg-red-600 h-5 aspect-square inline-block"></div>
        {" "} Red for Magnitudes Over 8
      </div>
      <div>
        <div className="bg-orange-400 h-5 aspect-square inline-block"> </div>
        {" "} Orange for Magnitudes Between 6 and 8
      </div>
      <div>
        <div className="bg-yellow-200 h-5 aspect-square inline-block"></div>
        {" "} Yellow for Magnitudes Below 6
      </div>
      <DataMap locations={locations}></DataMap>
    </div>
  );
}

export default Page;
