export type CountySearchParams = {
  nameQuery?: string;
  rangeQueries?: number[][];
  rangeFilterNames?: string[];
};
