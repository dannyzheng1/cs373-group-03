export const toolInstances = [
  {
    Name: "VS Code",
    Bio: "",
    Image:
      "https://uxwing.com/wp-content/themes/uxwing/download/brands-and-social-media/visual-studio-code-icon.png",
    Website: "https://code.visualstudio.com/",
  },
  {
    Name: "Next.js",
    Bio: "",
    Image:
      "https://testrigor.com/wp-content/uploads/2023/04/nextjs-logo-square.png",
    Website: "https://nextjs.org/",
  },
  {
    Name: "Namecheap",
    Bio: "",
    Image:
      "https://a.fsdn.com/allura/s/namecheap-logo-maker/icon?0deb429f557eb6b0d45800c789c82af2cde2f29480a43ad2d17c0e4fc9965979?&w=148",
    Website: "https://www.namecheap.com/",
  },
  {
    Name: "Amplify",
    Bio: "",
    Image:
      "https://pbs.twimg.com/profile_images/1114309924551417856/FKA4cm2x_400x400.png",
    Website: "https://aws.amazon.com/amplify/",
  },
  {
    Name: "Postman",
    Bio: "",
    Image: "https://cdn.worldvectorlogo.com/logos/postman.svg",
    Website: "https://www.postman.com/",
  },
  {
    Name: "TablePlus",
    Bio: "",
    Image:
      "https://d4.alternativeto.net/MbykiMl8WYeqMUHn584oCq_A9k85B532wgOkxeJLQ1k/rs:fit:280:280:0/g:ce:0:0/exar:1/YWJzOi8vZGlzdC9pY29ucy90YWJsZXBsdXNfMTQzNDE1LnBuZw.png",
    Website: "https://tableplus.com/",
  },
  {
    Name: "Flask",
    Bio: "",
    Image:
      "https://img.icons8.com/?size=256&id=TtXEs5SeYLG8&format=png",
    Website: "https://flask.palletsprojects.com/en/3.0.x/",
  },
  {
    Name: "Supabase",
    Bio: "",
    Image:
      "https://cf.appdrag.com/dashboard-openvm-clo-b2d42c/uploads/supabase-TAiY.png",
    Website: "https://supabase.com/",
  }, 
  {
    Name: "Gitlab CI/CD", 
    Bio: "", 
    Image: "https://spin.atomicobject.com/wp-content/uploads/gitlab-ci-cd-logo_2x.png",
    Website: "https://about.gitlab.com/",
  },
  {
    Name: "D3", 
    Bio: "", 
    Image: "https://avatars.githubusercontent.com/u/1562726?s=280&v=4",
    Website: "https://d3js.org/",
  },
  {
    Name: "TypeScript", 
    Bio: "", 
    Image: "https://www.svgrepo.com/show/374144/typescript.svg",
    Website: "https://www.typescriptlang.org/",
  },
  {
    Name: "Postgre SQL", 
    Bio: "", 
    Image: "https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/800px-Postgresql_elephant.svg.png",
    Website: "https://www.postgresql.org/",
  },

  
];
