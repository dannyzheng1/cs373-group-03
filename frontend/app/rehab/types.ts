export type RehabSeachParams = {
  searchName?: string;
  searchAddress?: string;
  searchCity?: string;
  searchCounty?: string;
  searchTreatment?: string;
  searchPayment?: string[];
  searchServices?: string[];
};
