export type ReentrySearchParams = {
  nameSearch?: string;
  citySearch?: string;
  countySearch?: string[];
  programType?: string[];
  RatingMin?: number;
  RatingMax?: number;
};
