import React from "react";

const temp = "animate-spin"; //make sure tailwind class is loaded

function Error() {
  return (
    <div className="w-full h-full flex flex-col items-center justify-center">
      <div>Error Loading</div>
    </div>
  );
}

export default Error;
